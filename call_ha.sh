#!/bin/bash

#~/Scripts/Hassio/toggle.sh switch ss_001 toggle

script_path="$( cd "$(dirname "$0")" ; pwd -P )"
token=$($script_path/token.sh)
domain=$($script_path/domain.sh)
url=$domain/api/services/$1/$3

curl -X POST -H "Authorization: Bearer "$token"" \
       -H "Content-Type: application/json" \
       -d '{"entity_id": "'$1.$2'"}' \
       $url