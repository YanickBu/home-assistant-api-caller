#!/bin/bash
tmp = pwd
script_path="$(cd "$(dirname "$0")" && pwd)"
cd script_path
# Domain
touch domain.sh
echo "Please input your domain"
read domain
echo \#\!/bin/bash\necho  = \"$domain\" > domain.py
# Token
touch token.sh
echo "Please input Home Assistant Token"
read token
echo \#\!/bin/bash\necho  = \"$token\" > token.py
cd $tmp