# home-assistant-api-caller

## Installation
1. Run install.sh
2. Input Domain name or IP
3. Input Token from HA interface

## Usage
### Switches
```console
call_ha.sh switch SWITCHNAME toggle
call_ha.sh switch SWITCHNAME turn_off
call_ha.sh switch SWITCHNAME turn_on
```
### Scripts
```console
call_ha.sh script SCRIPTNAME turn_on //Toggles script
```
